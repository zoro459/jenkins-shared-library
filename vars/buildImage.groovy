#!/usr/bin/env groovy
import com.example.Docker

def call(String imagename) {
    return new Docker(this).buildDockerImage(imagename) //this is allowing the Docker class to interact with the Jenkins pipeline.
}
