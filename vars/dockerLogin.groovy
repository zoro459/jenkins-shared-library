#!/usr/bin/env groovy
import com.example.Docker

def call() {
    return new Docker(this).dockerLogin() //this is allowing the Docker class to interact with the Jenkins pipeline.
}

