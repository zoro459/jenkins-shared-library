#!/usr/bin/env groovy
package com.example //This line defines the package in which the Docker class is located. The class is placed inside the com.example package.
class Docker implements Serializable { //it means that objects of that class can be saveing the state of the execution if the pipeline is pasued or resume 
        def script

        Docker(script) {
            this.script = script //allowing the Docker class to interact with the Jenkins pipeline.you establish a connection between the Docker class and the pipeline, enabling the class to execute pipeline steps and interact with the Jenkins pipeline's environment.
        }
        def buildDockerImage(String imagename) { //script is used to access Jenkins pipeline steps.
            script.sh "docker build -t $imagename ."

}
        def dockerLogin(){
            script.withCredentials([script.usernamePassword(credentialsId: 'docker', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                script.sh "echo $script.PASS | docker login -u $script.USER --password-stdin"
           
    }
        }
        def dockerPush(String imagename) {
                script.sh "docker push $imagename"
        
    
        }
} 
